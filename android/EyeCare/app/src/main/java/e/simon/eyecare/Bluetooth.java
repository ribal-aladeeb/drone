package e.simon.eyecare;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class Bluetooth {

    private MainActivity activity;
    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private InputStream inStream = null;

    private static final UUID MY_UUID =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static String address = "98:D3:31:FC:64:1A";

    public Bluetooth(MainActivity activity) {
        this.activity = activity;

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkState();
    }

    public void connect() {

        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        btAdapter.cancelDiscovery();

        try {
            btSocket.connect();
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }


        try {
            inStream = btSocket.getInputStream();
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and input stream creation failed:" + e.getMessage() + ".");
        }
    }

    public void deconnect() {
        if (inStream != null) {
            try {
                inStream.close();
            } catch (IOException e) {
                errorExit("Fatal Error", "In onPause() and failed to flush output stream: " + e.getMessage() + ".");
            }
        }

        try {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
    }

    public int getMessage() {
        InputStream tmpIn = null;
        try {
            tmpIn = btSocket.getInputStream();
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and input stream creation failed:" + e.getMessage() + ".");
        }

        int message = 0;
        byte[] buffer = new byte[1024];
        try {
            if (tmpIn.available() > 2) {
                try {
                    tmpIn.read(buffer);
                    message = buffer[0] & 0xFF;
                } catch (IOException e) {
                }
            }
        }catch(IOException e){}
        return message;
    }

    private void checkState() {
        if (btAdapter == null) {
            errorExit("Fatal Error", "Bluetooth Not supported. Aborting.");
        } else {
            if (!btAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                activity.finish();
            }
        }
    }

    private void errorExit(String title, String message) {
        System.out.println(title + message);
        System.exit(-1);
    }
}
