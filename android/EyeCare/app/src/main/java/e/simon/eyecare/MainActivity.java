package e.simon.eyecare;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.WindowManager;
import android.content.Context;
import android.os.Vibrator;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static long MAXDURATION = 255;
    private Bluetooth bt;

    private TextView txtDistance;
    private Vibrator v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtDistance = findViewById(R.id.txtDistance);

        bt = new Bluetooth(this);
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        bt.connect();
        update();
    }

    @Override
    public void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        bt.deconnect();
    }

    public void update() {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        synchronized (this) {
                            wait(500);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int distance = bt.getMessage();
                                    Resources res = getResources();
                                    if (distance != 0) {
                                        long[] pattern = {distance*3, (MAXDURATION - distance)*3};
                                        v.vibrate(pattern, -1);
                                        txtDistance.setText(res.getString(R.string.txt_distance, "" + (distance*2) + "cm"));
                                    Log.e("PRINTING","" + distance);
                                    }
                                }
                            });

                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
