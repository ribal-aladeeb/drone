#include <SoftwareSerial.h> 

const int echoPin = 10;
const int trigPin = 11;

const byte TxD = 1;
const byte RxD = 0;

long duration;
int distance;

SoftwareSerial mySerial(RxD, TxD);

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  Serial.begin(9600);   // for ide monitor
  mySerial.begin(9600); //for bluetooth
}

void loop() {
  int distanceToObject = computeDistanceFromSensor()/2;
  byte output;
  if(distanceToObject > 255){
   distanceToObject = 255;   
  }
  output = distanceToObject & 0xFF ;
  Serial.write(output);
  
}
int computeDistanceFromSensor(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);

  duration= pulseIn(echoPin, HIGH);

  distance = int(duration*0.034/2);
  return distance;
}

