#include <SoftwareSerial.h>

// Define the data transmit/receive pins in Arduino

#define TxD 1
#define RxD 0

// wired connections
#define HG7881_B_IA 10 // D10 --> Motor B Input A --> MOTOR B +
#define HG7881_B_IB 11 // D11 --> Motor B Input B --> MOTOR B -
 
// functional connections
#define MOTOR_B_PWM HG7881_B_IA // Motor B PWM Speed
#define MOTOR_B_DIR HG7881_B_IB // Motor B Direction
 
// the actual values for "fast" and "slow" depend on the motor
#define PWM_SLOW 50  // arbitrary slow speed PWM duty cycle
#define PWM_FAST 200 // arbitrary fast speed PWM duty cycle
#define DIR_DELAY 1000 // brief delay for abrupt motor changes

SoftwareSerial mySerial(RxD, TxD); // RX, TX for Bluetooth

void setup() {
  
  mySerial.begin(9600); // For Bluetooth
  
  Serial.begin(9600); // For the IDE monitor Tools -> Serial Monitor

  pinMode(LED_BUILTIN, OUTPUT);
  
  pinMode( MOTOR_B_DIR, OUTPUT );
  pinMode( MOTOR_B_PWM, OUTPUT );
  digitalWrite( MOTOR_B_DIR, LOW );
  digitalWrite( MOTOR_B_PWM, LOW );

}
void loop() {
  
    boolean isValidInput; do { byte c; // get the next character from the bluetooth serial port
    
    while ( !mySerial.available() ) ; // LOOP...

    c = mySerial.read();
   
    if (c == '0') {
      // Turn off LED
      digitalWrite(LED_BUILTIN, LOW);
    } else if (c == '1') {
      // Turn on LEFD
      digitalWrite(LED_BUILTIN, HIGH);
    }
    
    /*c = mySerial.read(); // Execute the option based on the character recieved
    
    Serial.print(c); // Print the character received to the IDE serial monitor
    
    switch ( c ) {
      
      case 'a':
        mySerial.println( "You've entered an 'a'" );

        digitalWrite( MOTOR_B_DIR, LOW );
        digitalWrite( MOTOR_B_PWM, LOW );
        delay( DIR_DELAY );
        // set the motor speed and direction
        digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
        analogWrite( MOTOR_B_PWM, 255-PWM_FAST ); // PWM speed = fast
        
        digitalWrite(LED_BUILTIN, HIGH);
        isValidInput = true;    
        break;
      
      case 'b':
        mySerial.println( "You've entered an 'b'" );

        digitalWrite( MOTOR_B_DIR, LOW );
        digitalWrite( MOTOR_B_PWM, LOW );
        delay( DIR_DELAY );
        // set the motor speed and direction
        digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
        analogWrite( MOTOR_B_PWM, 255-PWM_SLOW ); // PWM speed = slow
        
        digitalWrite(LED_BUILTIN, LOW);
        isValidInput = true;
        break;
      
      default:
        mySerial.println( "Please enter 'a' or 'b'" );
        isValidInput = false;
        break;
      
    }*/
    
  } while ( isValidInput == true ); // Repeat the loop

}
